using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : IStrategy
{
    private float _speed;

    public Rotate(float speed) => _speed = speed;
    public void Perform(Transform transform) => transform.Rotate(Vector3.forward, Time.deltaTime * _speed);

}
