using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Input : MonoBehaviour
{

    [SerializeField] private Performer _performer;

    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _rotateSpeed;
    [SerializeField] private int _emmitCount;

    public void Walk()
    {
        _performer.SetStrategy(new MoveForward(_moveSpeed));
    }

    public void Rotate()
    {
        _performer.SetStrategy(new Rotate(_rotateSpeed));
    }

    public void Emmit()
    {
        _performer.SetStrategy(new Emmit(_emmitCount));
    }
}
