using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emmit : IStrategy
{
    private int _particlesCount;

    public Emmit(int emmitCount) => _particlesCount = emmitCount;
    public void Perform(Transform transform)
    {
        ParticleSystem pt = Object.FindObjectOfType<ParticleSystem>();

        var main = pt.main;
        main.maxParticles = _particlesCount;

        pt.Emit(_particlesCount);

    }

}
